var i;
var number;

number = parseInt(prompt("Enter your number"));
console.log("The number you entered is: " + number);

for(i = number; i >= 0; i--) {
    // console.log(i);
        if(i <= 50) {
            break;
        }
        
        if(i % 10 === 0) {
            continue;
        }
        console.log("This number is divisible by 10. Skipping the number.");

        if(i % 5 === 0) {
            console.log(i);
        }
}


let theWord = "supercalifragilisticexpialidocious";

// for(let i = 0; i < theWord.length; i++) {

//     if(
//         theWord[i].toLowerCase() == "a" || 
//         theWord[i].toLowerCase() == "i" || 
//         theWord[i].toLowerCase() == "e" || 
//         theWord[i].toLowerCase() == "o" || 
//         theWord[i].toLowerCase() == "u"  
//         ) {
//             continue;
//         } else {
//             console.log(theWord[i]);
//         }
// }

let noVowels = theWord.replace(/[aeiou]/gi, '');
console.log(noVowels);
